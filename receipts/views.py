from django.shortcuts import render, redirect, get_object_or_404
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, NewExpenseForm, NewAccountForm

# Create your views here.

@login_required
def list_of_receipts(request):
    list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": list
    }
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "create": form,
    }
    return render(request, "receipts/create.html", context)


def category_list(request):
    list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": list
    }
    return render(request, "receipts/categories.html", context)


def account_list(request):
    list = Account.objects.all
    context = {
        "accounts": list
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = NewExpenseForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = NewExpenseForm()
    context = {
        "expense": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = NewAccountForm(request.POST)
        if form.is_valid():
            bank = form.save(False)
            bank.owner = request.user
            bank.save()
            return redirect("account_list")
    else:
        form = NewAccountForm()
    context = {
        "account": form,
    }
    return render(request, "receipts/create_account.html", context)